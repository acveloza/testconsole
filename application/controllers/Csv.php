<?php
class Csv extends CI_Controller
{
    public $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('csv_model');
    }
    function index()
    {
        $this->load->view('admin/uploadCsvView');
    }
    function uploadData()
    {
        $this->csv_model->uploadData();
        redirect('csv');
    }
    
    function doownloadfile()
    {
        $fileName = basename('SampleFile.csv');
        $filePath = base_url().'uploads/'.$fileName;
        if(!empty($fileName) && file_exists($filePath)){
            // Define headers
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Type: application/zip");
            header("Content-Transfer-Encoding: binary");
            
            // Read the file
            readfile($filePath);
            exit;
        }
        else{
            echo "not_valid =>".$fileName."<br> AND =>".$filePath;
        }
    }
}
?>